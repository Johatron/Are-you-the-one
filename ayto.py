
import copy

guys = ["Anthony", "Clinton", "Dimitri", "Ethan", "Joe", "Kareem", "Keith", "Malcom", "Michael", "Shad", "Tyler"]
girls = ["Alexis", "Alivia", "Audrey", "Diandra", "Geles", "Jada", "Keyana", "Nicole", "Nurys", "Uche", "Zoe"]

match_probabailities = [[0 for girl in girls] for guy in guys]

matching_ceremonies = [
    {
        'matches': {"Anthony": "Geles", "Clinton": "Uche", "Dimitri": "Diandra", "Ethan": "Jada", "Joe": "Zoe", "Kareem": "Alivia", "Keith": "Alexis", "Malcom": "Nurys", "Michael": "Keyana", "Shad": "Audrey", "Tyler": "Nicole"},
        'beams': 3
    },
    {
        'matches': {"Anthony": "Diandra", "Clinton": "Uche", "Dimitri": "Nicole", "Ethan": "Jada", "Joe": "Audrey", "Kareem": "Alivia", "Keith": "Alexis", "Malcom": "Nurys", "Michael": "Keyana", "Shad": "Geles", "Tyler": "Zoe"},
        'beams': 1
    },
    {
        'matches': {"Anthony": "Jada", "Clinton": "Uche", "Dimitri": "Nurys", "Ethan": "Alexis", "Joe": "Zoe", "Kareem": "Alivia", "Keith": "Diandra", "Malcom": "Geles", "Michael": "Audrey", "Shad": "Keyana", "Tyler": "Nicole" },
        'beams': 2
    },
    {
        'matches': {"Anthony": "Keyana", "Clinton": "Uche", "Dimitri": "Alexis", "Ethan": "Nicole", "Joe": "Zoe", "Kareem": "Diandra", "Keith": "Nurys", "Malcom": "Alivia", "Michael": "Geles", "Shad": "Audrey", "Tyler": "Jada"},
        'beams': 3
    },
    {
        'matches': {"Anthony": "Nicole", "Clinton": "Jada", "Dimitri": "Uche", "Ethan": "Geles", "Joe": "Zoe", "Kareem": "Alivia", "Keith": "Alexis", "Malcom": "Diandra", "Michael": "Nurys", "Shad": "Audrey", "Tyler": "Keyana"},
        'beams': 1
    },
    {
        'matches': {"Anthony": "Keyana", "Clinton": "Geles", "Dimitri": "Diandra", "Ethan": "Jada", "Joe": "Alexis", "Kareem": "Nurys", "Keith": "Zoe", "Malcom": "Alivia", "Michael": "Uche", "Shad": "Audrey", "Tyler": "Nicole"},
        'beams': 4
    },
    {
        'matches': {"Anthony": "Keyana", "Clinton": "Geles", "Dimitri": "Diandra", "Ethan": "Zoe", "Joe": "Uche", "Kareem": "Nurys", "Keith": "Jada", "Malcom": "Alexis", "Michael": "Audrey", "Shad": "Alivia", "Tyler": "Nicole"},
        'beams': 5
    },
    {
        'matches': {"Anthony": "Alivia", "Clinton": "Geles", "Dimitri": "Diandra", "Ethan": "Alexis", "Joe": "Jada", "Kareem": "Nurys", "Keith": "Audrey", "Malcom": "Uche", "Michael": "Keyana", "Shad": "Zoe", "Tyler": "Nicole", },
        'beams': 3
    }
]

truth_booths = [

    {
        'guy': "Anthony",
        'girl': "Geles",
        'result': False
    },
    {
        'guy': "Clinton",
        'girl': "Uche",
        'result': False
    },
    {
        'guy': "Dimitri",
        'girl': "Nicole",
        'result': False
    },
    {
        'guy': "Ethan",
        'girl': "Keyana",
        'result': False
    },
    {
        'guy': "Keith",
        'girl': "Alexis",
        'result': False
    },
    {
        'guy': "Keith",
        'girl': "Alivia",
        'result': False
    },
    {
        'guy': "Malcom",
        'girl': "Nurys",
        'result': False
    },
    {
        'guy': "Michael",
        'girl': "Audrey",
        'result': False
    },
    {
        'guy': "Tyler",
        'girl': "Nicole",
        'result': True
    }
]


def pick_guy(guys, girls, matches) :

    if(truth_booth_filter(matches) or matching_ceremony_filter(matches)) :
        return 0

    if(girls == []) :
        collect_stats(matches)
        return 1

    guy = guys[0]
    guys_copy = copy.deepcopy(guys)
    guys_copy.remove(guy)
    return pick_girl(guys_copy, girls, matches, guy)

def pick_girl(guys, girls, matches, guy) :

    possibilities = 0
    for girl in girls :
        girls_copy = copy.deepcopy(girls)
        girls_copy.remove(girl)
        matches_copy = copy.deepcopy(matches)
        matches_copy[guy] = girl
        possibilities += pick_guy(guys, girls_copy, matches_copy)

    return possibilities

def truth_booth_filter(matches) :

    for truth_booth in truth_booths :
        if(truth_booth['result'] == False and truth_booth['guy'] in matches) :
            if(matches[truth_booth['guy']] == truth_booth['girl']) :
                return True
        elif(truth_booth['result'] == True and truth_booth['guy'] in matches) :
            if(matches[truth_booth['guy']] != truth_booth['girl']) :
                return True
    return False

def matching_ceremony_filter(matches) :

    for ceremony in matching_ceremonies :
        similarity = sum([1 for boy, girl in matches.items() if(girl == ceremony['matches'][boy])])
        minimum_possible_similarity = 0 if ceremony['beams'] - (11 - len(matches)) < 0 else ceremony['beams'] - (11 - len(matches))
        if(similarity > ceremony['beams'] or similarity < minimum_possible_similarity):
            return True
    return False

def collect_stats(matches) :
    for guy, girl in matches.items() :
        match_probabailities[guys.index(guy)][girls.index(girl)] += 1

if __name__ == '__main__':
    pick_guy(copy.deepcopy(guys), copy.deepcopy(girls), {})
    for index, guy in enumerate(match_probabailities) :
        girl = guy.index(max(guy))
        print(guys[index] + ": " + girls[girl])
