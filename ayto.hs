
import qualified Data.Map as Map
import qualified Data.List as List
import qualified Data.Tuple as Tuple

total_number_of_couples = 11
global_truth_booths = [Map.fromList [("guy", "Ethan"), ("girl", "Keyana"), ("result", "False")]]
global_matching_ceremonies = [Map.fromList [("Anthony", "Geles"), ("Clinton", "Uche"), ("Dimitri", "Diandra"), ("Ethan", "Jada"), ("Joe", "Zoe"), ("Kareem", "Alivia"), ("Keith", "Alexis"), ("Malcom", "Nurys"), ("Michael", "Keyana"), ("Shad", "Audrey"), ("Tyler", "Nicole")]]
global_guys = ["Anthony", "Clinton", "Dimitri", "Ethan", "Joe", "Kareem", "Keith", "Malcom", "Michael", "Shad", "Tyler"]
global_girls = ["Alexis", "Alivia", "Audrey", "Diandra", "Geles", "Jada", "Keyana", "Nicole", "Nurys", "Uche", "Zoe"]

matching_ceremony_filter :: [Map.Map [Char] [Char]] -> Map.Map [Char] [Char] -> Int -> Bool
matching_ceremony_filter [ceremony] matches beams
	| (similarity matches ceremony) > beams = True
	| (similarity matches ceremony) < (minimum_possible_similarity matches beams) = True
	| otherwise = False

matching_ceremony_filter (ceremony:ceremonies) matches beams
	| (similarity matches ceremony) > beams = True
	| (similarity matches ceremony) < (minimum_possible_similarity matches beams) = True
	| otherwise = matching_ceremony_filter ceremonies matches beams

minimum_possible_similarity :: Map.Map [Char] [Char] -> Int -> Int
minimum_possible_similarity matches beams
	| minimum_similarity < 0 = 0
	| otherwise = minimum_similarity
	where minimum_similarity = beams - (total_number_of_couples - Map.size matches)

similarity :: Map.Map [Char] [Char] -> Map.Map [Char] [Char] -> Int
similarity matches ceremony = List.length (List.intersect (Map.toList matches) (Map.toList ceremony))

tb_girl_for_guy :: [Char] -> Map.Map [Char] [Char] -> [Char]
tb_girl_for_guy guy matches = match_girl
	where (Just match_girl) = Map.lookup guy matches

truth_booth_filter :: [Map.Map [Char] [Char]] -> Map.Map [Char] [Char] -> Bool
truth_booth_filter [truth_booth] matches
	| result == "True", Map.size matches == total_number_of_couples, (tb_girl_for_guy guy matches) /= girl = True
	| result == "False", Map.member guy matches, (tb_girl_for_guy guy matches) == girl = True
	| otherwise = False
	where (Just result) = Map.lookup "result" truth_booth
	      (Just guy) = Map.lookup "guy" truth_booth
	      (Just girl) = Map.lookup "girl" truth_booth

truth_booth_filter (truth_booth:truth_booths) matches
	| result == "True", Map.size matches == total_number_of_couples, (tb_girl_for_guy guy matches) /= girl = True
	| result == "False", Map.member guy matches, (tb_girl_for_guy guy matches) == girl = True
	| otherwise = truth_booth_filter truth_booths matches
	where (Just result) = Map.lookup "result" truth_booth
	      (Just guy) = Map.lookup "guy" truth_booth
	      (Just girl) = Map.lookup "girl" truth_booth

collect_stats :: Map.Map [Char] [Char] -> [[Int]] -> Int
collect_stats matches probabilities
	| Map.size matches == 1 =

pick_guy :: [[Char]] -> [[Char]] -> Map.Map [Char] [Char] -> [[Int]] -> Int
pick_guy [] [] matches probabilities
	| truth_booth_filter global_truth_booths matches = 0
	|	matching_ceremony_filter global_matching_ceremonies matches 3 = 0
	| otherwise = collect_stats matches probabilities

pick_guy [guy] girls matches probabilities
	| truth_booth_filter global_truth_booths matches = 0
	|	matching_ceremony_filter global_matching_ceremonies matches 3 = 0
	| otherwise = pick_girl [] [] girls matches guy probabilities

pick_guy (guy:remaining_guys) girls matches probabilities
	| truth_booth_filter global_truth_booths matches = 0
	|	matching_ceremony_filter global_matching_ceremonies matches 3 = 0
	| otherwise = pick_girl remaining_guys [] girls matches guy probabilities


pick_girl :: [[Char]] -> [[Char]] -> [[Char]] -> Map.Map [Char] [Char] -> [Char] -> [[Int]] ->  Int
pick_girl guys processed_girls [girl] matches guy probabilities = pick_guy guys processed_girls (match_couple guy girl matches) probabilities

pick_girl guys [] (girl:remaining_girls) matches guy probabilities =
		pick_guy guys remaining_girls (match_couple guy girl matches) probabilities +
		pick_girl guys [girl] remaining_girls (match_couple guy girl matches) guy probabilities

pick_girl guys processed_girls (girl:remaining_girls) matches guy probabilities =
		pick_guy guys (processed_girls ++ remaining_girls) (match_couple guy girl matches) probabilities +
		pick_girl guys (processed_girls ++ [girl]) remaining_girls (match_couple guy girl matches) guy probabilities

match_couple :: [Char] -> [Char] -> Map.Map [Char] [Char] -> Map.Map [Char] [Char]
match_couple guy girl matches = Map.insert guy girl matches

main = print(pick_guy
	["Anthony", "Clinton", "Dimitri", "Ethan", "Joe", "Kareem", "Keith", "Malcom", "Michael", "Shad", "Tyler"]
	["Alexis", "Alivia", "Audrey", "Diandra", "Geles", "Jada", "Keyana", "Nicole", "Nurys", "Uche", "Zoe"]
	Map.empty
	(take 11 (repeat (take 11 (repeat 0)))))
